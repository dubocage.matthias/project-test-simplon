import * as dotenv from 'dotenv';
dotenv.config()
import { App } from './src/app'

async function main() {
    const app = new App();
    app.registerRouter()
    await app.listen();
}

main();
