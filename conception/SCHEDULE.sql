-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema schedule
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `schedule` ;
-- -----------------------------------------------------
-- Schema schedule
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `schedule` DEFAULT CHARACTER SET utf8 ;
USE `schedule` ;

-- -----------------------------------------------------
-- Table `schedule`.`Event`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `schedule`.`Event` (
  `idEvent` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `startDate` DATETIME NOT NULL,
  `endDate` DATETIME NULL DEFAULT NULL,
  `description` TEXT NULL,
  PRIMARY KEY (`idEvent`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

INSERT INTO `schedule`.`Event` VALUES (1,'Marathon','2021-09-21 15:00:00','2021-09-22 15:00:00','Super Test ! YOUHOU');
-- -----------------------------------------------------
-- Table `schedule`.`User`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `schedule`.`User` (
  `idUser` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL DEFAULT NULL,
  `email` VARCHAR(45) NULL DEFAULT NULL,
  `password` VARCHAR(150) NULL DEFAULT NULL,
  `type` VARCHAR(45) NULL DEFAULT NULL,
  `salt` VARCHAR(45) NULL DEFAULT NULL,
  `Event_idEvent` INT(11) ,
  PRIMARY KEY (`idUser`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC),
  INDEX `fk_User_Event_idx` (`Event_idEvent` ASC),
  CONSTRAINT `fk_User_Event`
    FOREIGN KEY (`Event_idEvent`)
    REFERENCES `schedule`.`Event` (`idEvent`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

INSERT INTO `schedule`.`User` VALUES (1,'Test1','test@test.fr','azerty123456789','Test','SaltTest',1);

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
