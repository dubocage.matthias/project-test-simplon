import { connection } from "../../connectDB/ConnectDB";
import { Evenement } from  "../Models/Evenement";
import { ResultSetHeader, RowDataPacket } from "mysql2";


export class DAOEvenement{

    constructor(private db = connection) {}

    async getAll(): Promise<Evenement[]> {
            let [results] = await this.db.query<RowDataPacket[]>("SELECT * FROM event");
            return results.map(row => new Evenement(row['id'], row['name'], row['description'], row['startDate'], row['EndDate']));
    }

    async add(event:Evenement){
        try{
            let [result] = await this.db.query<ResultSetHeader>('INSERT INTO event (name,description,startDate,endDate) VALUES (?,?,?,?)', [
                event.getName(),
                event.getDescription(),
                event.getStartDate(),
                event.getEndDate(),
            ]);
            event.setId(result.insertId);
            return event;
        }catch(e){
            return e;
        }
    }

    async update(event:Evenement) {
        try{
            let [result] = await this.db.query<ResultSetHeader>('UPDATE event SET name = ?, startDate = ?, endDate = ?, description = ? WHERE idEvent = ?', [
                event.getName(),
                event.getStartDate(),
                event.getEndDate(),
                event.getDescription(),
                event.getId()
            ]);
            return result;
        } catch(e){
            return e;
        }
    }

    async getById(id:number){
        let [results] = await this.db.query<RowDataPacket[]>("SELECT * FROM event WHERE idEvent=?",[id]);
        if(results.length > 0) {
            return new Evenement(results['name'],results['starDate'],results['dateEnd'],results['description'],id);
        }
        return null;
    }
    
    async delete(event:Evenement){
        try{
            let [result] = await this.db.query<ResultSetHeader>('DELETE FROM event WHERE idEvent=?', [
                event.getId()
            ]);
            return result;
        } catch(e){
            return e;
        }
    }
}
