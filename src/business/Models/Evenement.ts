import {
    IsDate,
    IsNotEmpty,
    IsString
  } from 'class-validator';

export class Evenement{

    @IsString()
    @IsNotEmpty()
    private name:string;
    @IsDate()
    @IsNotEmpty()
    private startDate:Date;
    private description:string;
    private endDate:Date;
    private id:number;

    constructor(name:string,startDate:Date,endDate?:Date,description?:string,id?:number){
        this.id = id;
        this.name = name;
        this.description = description;
        this.startDate = startDate;
        this.endDate = endDate;
    }
    
    /**
     * @return id:number
     */
    getId():number{
        return this.id;
    }

    /**
     * @return name:string
     */
    getName():string{
        return this.name;
    }

    /**
     * @return description:string
     */
    getDescription():string{
        return this.description;
    }

    /**
     * @return date:Date
     */
    getStartDate():Date{
        return this.startDate;
    }
    /**
     * @return date:Date
     */
    getEndDate():Date{
        return this.endDate;
    }
    /**
     * @param id 
     */
    setId(id:number){
        this.id = id;
    }

    /**
     * @param name 
     */
    setName(name:string){
        this.name = name;
    }

    /**
     * @param description 
     */
    setDescription(description:string){
        this.description = description;
    }
    
    /**
     * 
     * @param date 
     */
    setStartDate(date:Date){
        this.startDate = date;
    }
    /**
     * 
     * @param date 
     */
    setEndDate(date:Date){
        this.endDate = date;
    }

    update(data:Evenement){
        Object.assign(this,data)
    }
}
