import { DAOEvenement } from "./DAOs/DAOEvenement";
import { Evenement } from "./Models/Evenement";

export class EvenementBusiness{

    private repo:DAOEvenement;

    constructor() {
        this.repo = new DAOEvenement();
    }

    async getAll() {

        let recipes = await this.repo.getAll();
        return recipes;
    }

    async add(data:string[]) {
        let event = new Evenement(data['name'],data['description'],data['date']data['id']);
        let recipes = await this.repo.add(event);
        return recipes;
    }

}
