import { DAOUser } from "../DAOs/DAOUser";
import { User } from "../Models/User";


export class UserStore {
    private repo:DAOUser
    constructor() {
        this.repo = new DAOUser();
    }
    async getAll() {

        let user = await this.repo.getAll();
        return user;
    }
    async getByID(id:number){
        let user = await this.repo.getByID(id);
        return user;
    }
    async add(user:User):Promise<User> {
        let newUser = await this.repo.add(user);      
        return newUser;
    }
    async update(user:User){
        let updateUser = await this.repo.update(user);
        return updateUser
    }

    async delete(user:User){
        await this.repo.delete(user)
        
        return user
    }
}
