import { DAOEvenement } from "../DAOs/DAOEvenement";
import { Evenement } from "../Models/Evenement";

export class StoreEvenement{

    private daoEvenement:DAOEvenement;

    constructor() {
        this.daoEvenement = new DAOEvenement();
    }

    async getAll() {
        let result = await this.daoEvenement.getAll();
        return result;
    }

    async add(event:Evenement){
        
        let result = await this.daoEvenement.add(event);
        if(typeof(result) === 'number')
        {
            return {'succes': true};
        }else{
            return {'error' : result};
        }
    }

    async getByID(id:number):Promise< Evenement | null >{
        let result = await this.daoEvenement.getById(id);
        return result;
    }

    async delete(id:number){
        let event = await this.getByID(id);
        let result = await this.daoEvenement.delete(event);
        return result;
    }

    async update(event:Evenement){
        let result = await this.daoEvenement.update(event);
        return result;
    }
}
