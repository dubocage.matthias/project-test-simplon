import { Request, Response, Router } from "express";
import { StoreEvenement } from "../business/Store/StoreEvenement";
import { Evenement } from "../business/Models/Evenement";
import { validate } from "class-validator";

export const evenementRouter = Router();
export const storeEvenement = new StoreEvenement();

evenementRouter.get("/evenements", async (req:Request, res:Response) => {
    try {
      let evenements = await storeEvenement.getAll();
      res.json(evenements);
    } catch (error) {
      res.status(500).json(error);
    }
});

evenementRouter.post("/evenement", async (req, res) => {
    const evenement = new Evenement(req.body.name, req.body.startDate, req.body.endDate, req.body.description);
    validate(evenement).then(errors =>{
        if (errors.length > 0) {
            res.status(400).json(errors)
        } else {
            storeEvenement.add(evenement).then(data => res.status(201).json(data));
        }
    });
});

evenementRouter.get("/evenement/:id", async (req, res) => {
    const event = await storeEvenement.getByID(Number.parseInt(req.params.id))
    res.json(event);
});

evenementRouter.patch('/evenement/:id', async (req, res) => {
    const event = new Evenement(req.body.name,new Date(req.body.startDate), new Date(req.body.endDate), req.body.description, Number.parseInt(req.params.id));
    validate(event).then(errors =>{
      if (errors.length > 0) {
        res.status(400).json(errors)
      } else {
        storeEvenement.update(event).then(result => {
            if(result instanceof Error ){
                res.status(400).json(result);
            }
            res.status(202).json(result);
        });  
        event.update(req.body)
      }
    });
});

evenementRouter.delete('/evenement/:id', async (req, res) => {
    storeEvenement.delete(Number.parseInt(req.params.id)).then( result => {
        if(result instanceof Error ){
            res.status(400).json(result);
        }
        res.status(202).json(result);
    });
})
