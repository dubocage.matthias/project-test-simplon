### USER STORIES ###
\
Membre :\
Visiteur :

</br>

- En tant que : Visiteur/Membre\
Je souhaite : consulter le planning\
Afin de : voir les évènements.

- En tant que : Membre\
Je souhaite : Créer un évènements\
Afin de : d'informer les visiteurs.

- En tant que : Membre\
Je souhaite : Modifier mon évènements\
Afin de : metre a jours les information de l'évènement.

- En tant que : Membre\
Je souhaite : Supprimer mon évènements\
Afin de : qu'il ne soit plus visible.

- En tant que : Visiteur\
Je souhaite : créer un compte\
Afin de : devenir Membre

- En tant que : Membre\
Je souhaite : m'authentifier\
Afin de : acceder au fonctionnaliter de l'agenda

</br></br>

### TESTS D'ACCEPTANCES ###

</br>

- Consulter le planning :
    - La date des évènements dans l'ordre chronologique au jour d'aujourd'hui

- Créer un évènements :
    - La date de l'évènement coincide pas avec une autre
    - Que la date de l'évènement ne soit pas passer.
    - Etre authentifié

- Modifier un évènements :
    - La date de l'évènement coincide pas avec une autre        
    - Que la date de l'évènement ne soit pas passer.
    - Être authentifié
    - Être le créateur de l'évènement

- Supprimer un évènements :
    - Être authentifié
    - Être le créateur de l'évènement

- Creer un compte :
    - Ne pas être authentifié
    - Un mail respectant la regex ([A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$)
    - Un password respectant la regex (^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{13,}$)
    - Un nom respectant la regex (^[A-Za-z\d]{3,}$)
    - Un Type respectant la regex (^[A-Za-z\d]{3,}$)
    - Que tous les champs soit non vide donc obligatoire

- L'authentification :
    - Un mail et un password qui soit lié à un membre stocker en base

### DIAGRAMME D'ENTITÉS

![diagClasseCalendar](conception/diagClasse/diagClasseCalendar.png)