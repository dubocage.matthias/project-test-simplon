import { userRouter } from "../../src/controller/UserController";
import { expect, spy, use } from "chai";
import * as chaiSpy from "chai-spies";
import { Request, Response } from "express";
import { User } from "../../src/business/Models/User";
import { DAOUser } from "../../src/business/DAOs/DAOUser";
import { connection } from "../../src/connectDB/ConnectDB";
use(chaiSpy);

describe("User Controller", () => {
  beforeEach(async () => {
    await connection.query("START TRANSACTION");
  });
  afterEach(async () => {
    await connection.query("ROLLBACK");
  });
  describe("getAll method", () => {
    it("Should return users in json format", async () => {
      spy.on(DAOUser,'getAll', async ()=>[
        new User('Test1','test@test.fr','azerty123456789','Test','SaltTest'),
      ]);
      const response = {
        json:spy()
      };
      await userRouter.get('/users',(req,res)=> {
        expect(response.json).to.have.been.called.with([
          new User('Test1','test@test.fr','azerty123456789','Test','SaltTest')
        ]);
      })
    });
  });
});
