import { evenementRouter } from "../../src/controller/EvenementController";
import request = require("supertest");


describe('EvenementController',()=>{
    describe('getAll methods', () => {
        it('Should return events in json format', async (done) => {
          await request(evenementRouter)
            .get('/evenements')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200, done);
        });
      })
})
