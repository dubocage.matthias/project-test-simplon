import { User } from "../../../src/business/Models/User";
import { expect } from 'chai';

describe('User Model', () => {
    let user: User;
    beforeEach(() => {
        user = new User('Test','aaaa@aaa.fr','azerty','association', 'kfbvkjhsdbcqosdcbq');
    })

    it('Should return a name', () =>{
        expect(user.getName()).to.be.equal('Test');
        expect(user.getEmail()).to.be.equal('aaaa@aaa.fr');
        expect(user.getType()).to.be.equal('association');
    });
});
