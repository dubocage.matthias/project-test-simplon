import { User }  from "../../../src/business/Models/User";
import { DAOUser } from "../../../src/business/DAOs/DAOUser";
import { expect } from "chai";
import { connection } from "../../../src/connectDB/ConnectDB";

describe('DAO User',()=>{

    let user:User;
    let daoUser: DAOUser;

    beforeEach(async() => {
        await connection.query('START TRANSACTION;')
        user = new User('gile', 'gille@oups.fr' , 'YOUHOU' , 'membre','SaltTest123456789');
        daoUser = new DAOUser();
    });

    afterEach(async() => {
        await connection.query('ROLLBACK');
    });

    it('Should call getAll method in DAOUser', async () =>{
        const users = await daoUser.getAll()
        expect(users.length).to.be.equal(1);
        console.log(users[0])
        expect(users[0].getName()).to.be.equal('Test1')
        expect(users[0].getEmail()).to.be.equal('test@test.fr')
        expect(users[0].getType()).to.be.equal('Test')
    });


    it('Should insert a new user in database return an ID', async () =>{
        user = await daoUser.add(user);
        expect(user.id).to.be.a('number');

    });
    
    it('Should modify a user in database and return that 1 Row was modified', async()=>{
        const userModified = await daoUser.getByID(1)
        userModified.setType('bloups')
        const update = await daoUser.update(userModified);        
        expect(update[0].affectedRows).to.be.equals(1);
    });
    it('Should insert and delete a user in the database and return an affected row', async ()=>{
        user = await daoUser.add(user);
        const deleted =  await daoUser.delete(user)
        expect(deleted.affectedRows).to.be.equal(1)
    });
})
