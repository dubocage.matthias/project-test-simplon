import { Evenement }  from "../../../src/business/Models/Evenement";
import { DAOEvenement } from "../../../src/business/DAOs/DAOEvenement";
import { expect } from "chai";
import { connection } from "../../../src/connectDB/ConnectDB";
import { ResultSetHeader } from "mysql2";

describe('DAOEvenement',()=>{

    let evenement : Evenement;
    let daoEvenement : DAOEvenement;

    beforeEach(async() => {
        await connection.query('START TRANSACTION;')
        daoEvenement = new DAOEvenement();
        evenement = new Evenement( 'Marathon', new Date('September 21, 2021 15:00:00'), new Date('September 22, 2021 15:00:00'), 'Super Test ! YOUHOU');
        let [result] = await connection.query<ResultSetHeader>('INSERT INTO Event (name,description,startDate,endDate) VALUES (?,?,?,?)', [
            evenement.getName(),
            evenement.getDescription(),
            evenement.getStartDate(),
            evenement.getEndDate(),
        ]);;
        evenement.setId(result.insertId);
    })

    afterEach(async() => {
       await connection.query('ROLLBACK;')
    })

    it('Should intentied a DAOEvenement', () =>{
        expect(daoEvenement).to.be.a('object');
    })

    it('Should insert a new event in database return number', async () =>{
        const insert = await daoEvenement.add(evenement);
        evenement.setId(insert.getId());
        expect(insert.getId()).to.be.a('number');
    });


    it('Should insert a bad event in database return error', async()=>{
        evenement.setName(null);
        evenement.setStartDate(null);
        const insert = await daoEvenement.add(evenement);
        expect(insert).to.be.a('Error');
    });

    it('Should modif an event in database and return true', async()=>{
        evenement.setName('tota');
        evenement.setStartDate(new Date('September 21, 2021 16:00:00'));
        evenement.setEndDate(new Date('September 21, 2021 17:00:00'));
        evenement.setDescription('Update');
        const update = await daoEvenement.update(evenement);
        expect(update.changedRows).to.be.equals(1);
    })

    it('Should modify an event in database and return false', async()=>{
        const update = await daoEvenement.update(evenement);
        expect(update.changedRows).to.be.equals(0);
    })

    it('should delete an event in a database', async () =>{
        const deleteEvent = await daoEvenement.delete(evenement);
        expect(deleteEvent.affectedRows).to.be.equals(1);
    })
})
