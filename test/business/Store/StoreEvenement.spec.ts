import { expect } from "chai";
import { StoreEvenement } from "../../../src/business/Store/StoreEvenement";
import { Evenement } from "../../../src/business/Models/Evenement";
import { connection } from "../../../src/connectDB/ConnectDB";

describe('Store Evenement',()=>{
    let storeEvenement : StoreEvenement;
    let request: Evenement;

    beforeEach(async()=>{
        await connection.query('START TRANSACTION;')
        storeEvenement = new StoreEvenement();
        request = new Evenement('matthias', new Date('September 22, 2021 15:00:00'), new Date('September 22, 2021 15:00:00') , 'super test');
    });

    afterEach(async() => {
        await connection.query('ROLLBACK;')
    })

    it('Should intentied a StoreEvenement', () =>{
        expect(storeEvenement).to.be.a('object');
    })

    it('Should add a new Event in database and return succes true ', async() =>{
        const result = await storeEvenement.add(request);

        expect(result).to.be.deep.equal({'succes':true});
    });

    it('Should add a new Event in database and return error', async() =>{
        const result = await storeEvenement.add(request);

        expect(result).to.be.deep.equal({'error': 'Insert fail'});
    });

    it('Should get all events', async() =>{
        const result = await storeEvenement.getAll();
        expect(result).to.be.an('array');
    });

})
