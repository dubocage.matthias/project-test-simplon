import { expect } from "chai";
import { User } from "../../../src/business/Models/User";
import { UserStore } from "../../../src/business/Store/StoreUser";
import { connection } from "../../../src/connectDB/ConnectDB";

describe('Store User',()=>{
    let userStore : UserStore;
    let user: User;

    beforeEach(async()=>{
        await connection.query('START TRANSACTION;')
        userStore = new UserStore();
        user = new User('gile', 'gille@oups.fr' , 'YOUHOU' , 'membre','SaltTest123456789');
    });

    afterEach(async() => {
        await connection.query('ROLLBACK;')
    })

    it('Should create an instance of UserStore', () =>{
        expect(userStore).to.be.a('object');
    })

    it('Should add a new User in database and return his ID.', async() =>{
        const createdUser = await userStore.add(user);

        expect(createdUser).to.be.a('object');
    });

    it('Should find a User in database and return the User', async() =>{
        const result = await userStore.getByID(1);

        expect(result).to.be.a('object');
    });

    it('Should get all users', async() =>{
        const result = await userStore.getAll();
        expect(result).to.be.an('array');
    });

})
